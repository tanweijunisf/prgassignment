﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelApp
{
    class Stay
    {
        private List<HotelRoom> roomlist = new List<HotelRoom>();

        public List<HotelRoom> Roomlist
        {
            get { return roomlist; }
            set { roomlist = value; }
        }

        private DateTime checkInDate;

        public DateTime CheckInDate
        {
            get { return checkInDate; }
            set { checkInDate = value; }
        }

        private DateTime checkOutDate;

        public DateTime CheckOutDate
        {
            get { return checkOutDate; }
            set { checkOutDate = value; }
        }

        public Stay() { }

        public Stay(DateTime ci, DateTime co)
        {
            CheckInDate = ci;
            CheckOutDate = co;
            
        }

        public void AddRoom(HotelRoom r1)
        {
            Roomlist.Add(r1);
        }

        public double CalculateTotal()
        {
            double total = 0;
            double datediff = (CheckOutDate - CheckInDate).TotalDays;
            foreach (HotelRoom i in Roomlist)
            {
                total += datediff * i.CalculateCharges();
            }
            return total;
        }

        public override string ToString()
        {
            return "hi";
        }
    }
}
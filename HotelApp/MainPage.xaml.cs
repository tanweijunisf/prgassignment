﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace HotelApp
{
   
    
    
    /// <summary>
    /// 
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        List<HotelRoom> hotelList = new List<HotelRoom>();
        List<HotelRoom> availrooms = new List<HotelRoom>();
        List<HotelRoom> selectedroom = new List<HotelRoom>();
        List<Guest> guestList = new List<Guest>();
        List<string> Failmsg = new List<string> { "No Room Found,have you checked in?" };

        
        
        public void InitData()
        {
            HotelRoom s1 = new StandardRoom("Standard", "101", "Single", 90.0, true, 1);
            HotelRoom s2 = new StandardRoom("Standard", "102", "Single", 90.0, true, 1);
            HotelRoom s3 = new StandardRoom("Standard", "201", "Twin", 110.0, true, 2);
            HotelRoom s4 = new StandardRoom("Standard", "202", "Twin", 110.0, true, 2);
            HotelRoom s5 = new StandardRoom("Standard", "203", "Twin", 110.0, true, 2);
            HotelRoom s6 = new StandardRoom("Standard", "301", "Triple", 120.0, true, 3);
            HotelRoom s7 = new StandardRoom("Standard", "302", "Triple", 120.0, true, 3);
            HotelRoom d1 = new DeluxeRoom("Deluxe", "204", "Twin", 140.0, true, 2);
            HotelRoom d2 = new DeluxeRoom("Deluxe", "205", "Twin", 140.0, true, 2);
            HotelRoom d3 = new DeluxeRoom("Deluxe", "303", "Triple", 210.0, true, 3);
            HotelRoom d4 = new DeluxeRoom("Deluxe", "304", "Triple", 210.0, true, 3);
            hotelList.Add(s1);
            hotelList.Add(s2);
            hotelList.Add(s3);
            hotelList.Add(s4);
            hotelList.Add(s5);
            hotelList.Add(s6);
            hotelList.Add(s7);
            hotelList.Add(d1);
            hotelList.Add(d2);
            hotelList.Add(d3);
            hotelList.Add(d4);
            
        }
        public MainPage()
        {
            InitData();
            

            this.InitializeComponent();

        }

        private void ChkrmBtn_Click(object sender, RoutedEventArgs e)
        {

            availrooms.Clear();
            foreach (HotelRoom r in hotelList)
            {
                if (r.IsAvail)
                {
                    availrooms.Add(r);

                }
            }
            availrmLv.ItemsSource = availrooms;
        }

        private void AddrmBtn_Click(object sender, RoutedEventArgs e)
        {
            HotelRoom chosen = (HotelRoom)availrmLv.SelectedItem;

            if (chosen is StandardRoom)
            {
                StandardRoom s = (StandardRoom)chosen;
                if (wifi.IsChecked == true)
                {
                    s.RequireWifi = true;
                }
                if (bfast.IsChecked == true)
                {
                    s.RequireBreakfast = true;
                }
                s.CalculateCharges();
            }
            else if (chosen is DeluxeRoom)
            {
                DeluxeRoom d = (DeluxeRoom)chosen;
                if (bed.IsChecked == true)
                {
                    d.AdditionalBed = true;
                }
                d.CalculateCharges();
            }
            foreach (HotelRoom r in hotelList)
            {
                if (r == chosen)
                {
                    chosen.IsAvail = false;
                }
            }
            selectedroom.Add(chosen);
            availrmLv.ItemsSource = null;
            selectrmLv.ItemsSource = null;
            availrmLv.ItemsSource = hotelList;
            selectrmLv.ItemsSource = selectedroom;


        }

        private void ExtendBtn_Click(object sender, RoutedEventArgs e)
        {

        }

        private void SearchBtn_Click(object sender, RoutedEventArgs e)
        {
            
            availrmLv.ItemsSource = null;
            string searchname = guestTxt.Text;
            string searchpassno = ppTxt.Text;
            foreach (Guest g in guestList)
            {
                bool bfastcount = false;
                bool wificount = false;
                bool bedcount = false;
                
                if (g.Name == searchname || g.PpNumber == searchpassno)
                {
                    double total = 0;
                    invoicedisplay.Text = "";
                    availrmLv.ItemsSource = g.HotelStay.Roomlist;
                    foreach (HotelRoom r in g.HotelStay.Roomlist)
                    {

                        
                        int datediff = (checkOutDate.Date.Value.Date - checkInDate.Date.Value.Date).Days;
                        
                        double subtotal = r.DailyRate * datediff;
                        if (r is StandardRoom)
                        {
                            StandardRoom q = (StandardRoom)r;
                            if (q.RequireBreakfast)
                            {
                                bfastcount = true;
                            }
                            if (q.RequireWifi)
                            {
                                wificount =true;
                            }
                            total += subtotal;
                            invoicedisplay.Text += "Room Type:" + r.RoomType + "\nRoom No.:" + r.RoomNumber + "\nNo.of Nights:" + datediff + "\nWifi:" + wificount + "\nBreakfast:" + bfastcount + "\nSubtotal:" + subtotal+"\n\n";
                        }
                        else if(r is DeluxeRoom)
                        {
                            DeluxeRoom z = (DeluxeRoom)r;
                            if (z.AdditionalBed)
                            {
                                bedcount = true;
                            }
                            total += subtotal;
                            invoicedisplay.Text += "Room Type:" + r.RoomType + "\nRoom No.:" + r.RoomNumber + "\nNo.of Nights:" + datediff + "\nAdditional Beds:"+bedcount + "\nSubtotal:" + subtotal+"\n\n";
                        }          
                    }
                    invoicedisplay.Text += "\n\nTotal:" + total+"\n\n\n";
                }
                else
                {
                    availrmLv.ItemsSource = Failmsg;
                }
            }
        }

        private void SelectrmLv_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void AvailrmLv_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            bed.IsChecked = false;
            wifi.IsChecked = false;
            bfast.IsChecked = false;
            bed.IsEnabled = true;
            wifi.IsEnabled = true;
            bfast.IsEnabled = true;
            HotelRoom chosen = (HotelRoom)availrmLv.SelectedItem;
            if(chosen is StandardRoom)
            {
                bed.IsEnabled = false;
            }
            else
            {
                wifi.IsEnabled = false;
                bfast.IsEnabled = false;
            }
        }

        private void AdultnoTxt_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void ChkoutBtn_Click(object sender, RoutedEventArgs e)
        {

        }
        private int CreatePerson(string g)
        {
            if (guestList.Count == 0)
            {
                return 1;
            }
            else
            {
                int i = 1;
                foreach(Guest z in guestList)
                {
                    if (z.Name == g || z.PpNumber == g)
                    {
                        i=0;
                        break;
                    }
                }
                return i;
            }
        }

        private void ChkinBtn_Click(object sender, RoutedEventArgs e)
        {
            string name = guestTxt.Text;
            string passno = ppTxt.Text;
            Stay stay1 = new Stay(checkInDate.Date.Value.DateTime, checkOutDate.Date.Value.DateTime);         
            
            Membership m1 = new Membership("Ordinary", 0);
            if (CreatePerson(name) == 1 || CreatePerson(passno)==1)
            {

                Guest g1 = new Guest(name, passno, stay1, m1, true);
                foreach(HotelRoom r in selectedroom)
                {
                    g1.HotelStay.AddRoom(r);
                }
                
                guestList.Add(g1);
            }
            else
            {
                foreach(Guest g in guestList)
                {
                    if (g.Name == name ||g.PpNumber == passno)
                    {
                        foreach(HotelRoom r in selectedroom)
                        {
                            g.HotelStay.AddRoom(r);
                        }
                    }
                }
            }
            
            selectedroom.Clear();
            selectrmLv.ItemsSource = null;
        }

        private void RemovermBtn_Click(object sender, RoutedEventArgs e)
        {

            HotelRoom removechosen = (HotelRoom)selectrmLv.SelectedItem;
            foreach (HotelRoom r in hotelList)
            {
                if (r == removechosen)
                {
                    removechosen.IsAvail = true;
                }
            }

            selectedroom.Remove(removechosen);
            selectrmLv.ItemsSource = null;
            availrmLv.ItemsSource = null;
            selectrmLv.ItemsSource = selectedroom;
            availrmLv.ItemsSource = hotelList;

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelApp
{
    class DeluxeRoom : HotelRoom
    {
        private bool additionalBed;

        public bool AdditionalBed
        {
            get { return additionalBed; }
            set { additionalBed = value; }
        }

        public DeluxeRoom() { }

        public DeluxeRoom(string rt, string rn, string bc, double dr, bool avail, int num) : base("Deluxe",rn,bc,dr,avail,num)
        {

        }

        public override double CalculateCharges()
        {
            double surcharges = 0;
            if (AdditionalBed)
            {
                surcharges = 25;
            }
            else
            {
                surcharges = 0;
            }
            DailyRate += surcharges;
            return DailyRate;
        }
        public override string ToString()
        {
            return base.ToString();
        }

    }
}
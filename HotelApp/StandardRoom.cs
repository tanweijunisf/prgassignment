﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelApp
{
    class StandardRoom : HotelRoom
    {
        private bool requireWifi;
        private bool requireBreakfast;
        public bool RequireWifi
        {
            get { return requireWifi; }
            set { requireWifi = value; }
        }
        public bool RequireBreakfast
        {
            get { return requireBreakfast; }
            set { requireBreakfast = value; }
        }
        public StandardRoom() { }

        public StandardRoom(string t, string r, string b, double d, bool i, int n) : base("Standard",r,b,d,i,n)
        {

        }

        public override double CalculateCharges()
        {
            double surcharge = 0;
            if (RequireWifi)
            {
                surcharge += 10.0;
            }
            if (RequireBreakfast)
            {
                surcharge += 20;
            }
            DailyRate += surcharge;
            return DailyRate;
        }

    }
}
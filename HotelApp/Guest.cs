﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelApp
{
    class Guest
    {
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private string ppNumber;

        public string PpNumber
        {
            get { return ppNumber; }
            set { ppNumber = value; }
        }

        private Stay hotelStay;

        public Stay HotelStay
        {
            get { return hotelStay; }
            set { hotelStay = value; }
        }

        private Membership membership;

        public Membership Membership
        {
            get { return membership; }
            set { membership = value; }
        }

        private bool isCheckedIn;

        public bool IsCheckedIn
        {
            get { return isCheckedIn; }
            set { isCheckedIn = value; }
        }

        public Guest() { }

        public Guest(string n, string p, Stay hs, Membership m, bool ic)
        {
            this.Name = n;
            this.ppNumber = p;
            this.HotelStay = hs;
            this.Membership = m;
            this.IsCheckedIn = ic;
        }

        public override string ToString()
        {

            return hotelStay.ToString();
        }
    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelApp
{
    abstract class HotelRoom
    {
        private string roomType;

        public string RoomType
        {
            get { return roomType; }
            set { roomType = value; }
        }

        private string roomNumber;

        public string RoomNumber
        {
            get { return roomNumber; }
            set { roomNumber = value; }
        }

        private string bedConfiguration;

        public string BedConfiguration
        {
            get { return bedConfiguration; }
            set { bedConfiguration = value; }
        }

        private double dailyRate;

        public double DailyRate
        {
            get { return dailyRate; }
            set { dailyRate = value; }
        }

        private bool isAvail;

        public bool IsAvail
        {
            get { return isAvail; }
            set { isAvail = value; }
        }

        private int noOfOccupants;

        public int NoOfOccupants
        {
            get { return noOfOccupants; }
            set { noOfOccupants = value; }
        }

        public HotelRoom() { }

        public HotelRoom(string rt, string rn, string bc, double dr, bool avail, int num)
        {
            RoomType = rt;
            RoomNumber = rn;
            BedConfiguration = bc;
            DailyRate = dr;
            IsAvail = avail;
            NoOfOccupants = num;
        }

        public abstract double CalculateCharges();

        public override string ToString()
        {
            return RoomType + "\t\t" + RoomNumber + "\t" + BedConfiguration + "\t" + Convert.ToString(DailyRate)+"\t"+NoOfOccupants;
        }

    }
}